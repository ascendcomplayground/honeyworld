<?php

// sentence.php

return [
  'total_honey_earned' => '蜂蜜总共获得',
  'welcome' => '欢迎!',
  'click_for_info' => '点击以获得更多资讯',
  'logout' => '登出',
  'scan' => "扫描QR码",
  'play' => '游玩',
  'login' => '登入',
];