<?php

return [

    'register' => '注册',
    'fullname' => '全名',
    'username' => '昵称',
    'password' => '密码',
    'email' => '电子邮件(可选)',
    'phone' => '电话号码',
    'back_to_login' => '返回等人',
    'recover_email' => '请填写已注册电子邮件',
    'recover_phone' => '请填写已注册电话号码',
    'submit' => '提交',
    'home' => '首页',
];