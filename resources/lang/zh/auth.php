<?php

return [
    'login' => '登入',
    'username' => '昵称',
    'password' => '密码',
    'register' => '注册',
    'forget' => '忘记昵称/密码',
    'home' => '首页'
];
