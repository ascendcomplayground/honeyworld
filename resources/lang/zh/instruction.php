<?php

// sentence.php

return [
  'total_honey_earned' => 'Total Honey Earned',
  'welcome' => 'Welcome User!',
  'click_for_info' => 'Click here for more information',
  'logout' => 'Logout',
  'scan' => "Scan QR code",
  'play' => 'Play',
  'login' => 'login',
];