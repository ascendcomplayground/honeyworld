<?php

return [

    'register' => 'Register',
    'fullname' => 'Fullname',
    'username' => 'Username',
    'password' => 'Password',
    'email' => 'Email(optional)',
    'phone' => 'Phone',
    'back_to_login' => 'Back to login',
    'recover_email' => 'Please fill in your registered Email',
    'recover_phone' => 'Please fill in your registered Phone Number',
    'submit' => 'Submit',
    'home' => 'Home',

];
