<?php

// sentence.php

return [
  'total_honey_earned' => 'Total Honey Earned',
  'welcome' => 'Welcome User!',
  'click_for_info' => 'Click here for more information',
  'logout' => 'Logout',
  'scan' => "Scan Recipt",
  'play' => 'Play',
  'login' => 'Login',
  'register' => 'Register'
];