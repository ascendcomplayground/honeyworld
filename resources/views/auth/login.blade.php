@extends('layouts.app')

@section('content')
<div class="ui horizontal divider">
    {{ trans('auth.login')}}
</div>
<div class="form-container border-solid">
    <form method="POST" action="{{ route('login') }}">
        <div class="form-column text-center">
            <div class="username attribute">
                <label for="name">{{ trans('auth.username')}}</label>
                <div class="username-input">
                    <input id="username" type="username" class="form-control @error('name') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                    @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            @csrf
            <div class="password attribute">
                <label for="password">{{ trans('auth.password')}}</label>
                <div class="password-input">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="login-register attribute flex">
                <div>
                    <button type="submit" class="btn btn-primary">
                        {{ trans('auth.login')}}
                    </button>
                </div>
                <div>
                    <a href="{{ route('register') }}">
                        <div class="btn btn-primary">
                            {{ trans('auth.register')}}
                        </div>
                    </a>
                </div>
            </div>
            @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
                <div class="btn btn-link">
                    {{ trans('auth.forget')}}
                </div>
            </a>
            @endif
            <a href="../" class="home-bottom">
                <div class="btn btn-primary">
                    {{ trans('auth.home')}}
                </div>
            </a>
        </div>
    </form>
</div>
@endsection