@extends('layouts.app')

@section('content')
<div class="ui horizontal divider">
    Forget Username/Password
</div>
<div class="form-container border-solid">
    <p class="text-center">Recover by</p>
    <div class="btn-group btn-group-toggle flex justify-center align-center" data-toggle="buttons">
        <label class="btn btn-secondary active">
            <input type="radio" name="email" id="email" autocomplete="off" checked> Email
        </label>
        <label class="btn btn-secondary">
            <input type="radio" name="phone" id="phone" autocomplete="off"> Phone
        </label>
    </div>
    <div class="recover_by_email">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-column text-center">
                <div class="username attribute">
                    <label for="username">{{ trans('validation.recover_email')}}</label>
                    <div class="username-input">
                        <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="recover_by_phone hide-div">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-column text-center">
                <div class="phone attribute">
                    <label for="phone">{{ trans('validation.recover_phone')}}</label>
                    <div class="phone-input">
                        <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" required autocomplete="phone">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="action attribute flex align-center justify-center">
        <div class="register-action">
            <button type="submit" class="btn btn-primary">
                {{ trans('validation.submit')}}
            </button>
        </div>
        <a href="../" class="home-bottom">
            <div class="btn btn-primary">
                {{ trans('validation.home')}}
            </div>
        </a>
    </div>
</div>
@endsection
