@extends('layouts.app')

@section('content')

    <div class="ui horizontal divider">
        {{ trans('validation.register')}}
    </div>
<div class="form-container border-solid">
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-column text-center">
            <div class="fullname attribute">
                <label for="fullname">{{ trans('validation.fullname')}}</label>
                <div class="fullname-input">
                    <input id="fullname" type="fullname" class="form-control @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" required autocomplete="fullname" autofocus>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="username attribute">
                <label for="username">{{ trans('validation.username')}}</label>
                <div class="username-input">
                    <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            @csrf
            <div class="password attribute">
                <label for="password">{{ trans('validation.password')}}</label>
                <div class="password-input">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="email attribute">
                <label for="email">{{ trans('validation.email')}}</label>
                <div class="email-input">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" autocomplete="email">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="phone attribute">
                <label for="phone">{{ trans('validation.phone')}}</label>
                <div class="phone-input">
                    <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" required autocomplete="phone">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="action attribute">
                <div class="register-action">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('validation.register')}}
                    </button>
                </div>
            </div>
            <a href="../login" class="home-bottom">
                <div class="btn btn-primary">
                    {{ trans('validation.back_to_login')}}
                </div>
            </a>
        </div>
    </form>
</div>
@endsection