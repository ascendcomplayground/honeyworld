@extends('layouts.app')

@section('content')

<div class="form-container border-solid">
    <div class="welcome_layout flex flex-column align-center justify-center text-center">
        <div class="welcome-description">
            Welcome back, User
        </div>
        <div class="flex flex-row align-center justify-space-between text-center">
            <div class="border-right-solid width-one_thirds">
                <div>Rank</div>
                <div>35</div>
            </div>
            <div class="border-right-solid width-one_thirds">
                <div>Play Token</div>
                <div>3</div>
            </div>
            <div class="width-one_thirds">
                <div>Total Score</div>
                <div>11450</div>
            </div>
        </div>
    </div>
    <div class="first_page ">
        <div class="ui horizontal divider">
            {{ trans('instruction.topic')}}
        </div>
        <div class="first_page_flex">
            <div class="instuction-description ">
                Dispense the honey liquid by pressing the button on your mobile interface to fill up the honey jug, honey is previous so only dispense the honey when the jug is below the dispenser. Avoid dispensing the honey liquid when the bee is obstructing the way.
            </div>
            <div class="text-center">
                1/4
            </div>
            <div class="action attribute flex align-center justify-center">
                <div id="link_to_second_page" class="btn btn-primary">
                    {{ trans('instruction.next')}}
                </div>
                <a href="../" class="home-bottom">
                    <div class="btn btn-primary">
                        {{ trans('validation.home')}}
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="second_page hide-div">
        <div class="ui horizontal divider">
            Barcode
        </div>
        <form>
            <div class="input-field">
                <input id="isbn_input" class="isbn" type="text" placeholder="Tracking barcode" />
                <button type="button" class="icon-barcode button scan">Open</button>
            </div>
        </form>
        <div id="scanner_layout"></div>
        <div class="text-center">
            Scan Recipt Barcode to start game
        </div>
        <br>
        <div class="text-center">
            2/4
        </div>
        <div class="action attribute flex align-center justify-center">
            <div id="link_to_third_page" class="btn btn-primary">
                {{ trans('instruction.next')}}
            </div>
            <a href="../" class="home-bottom">
                <div class="btn btn-primary">
                    {{ trans('validation.home')}}
                </div>
            </a>
        </div>
    </div>
    <div class="third_page hide-div">
        <div class="ui horizontal divider">
            QR code
        </div>
        <div class="flex justify-center">
            <input type=text size=16 placeholder="Tracking QR code" class=qrcode-text>
            <label class=qrcode-text-btn>
                <input type=file accept="image/*" capture=environment onchange="openQRCamera(this)" tabindex=-1>
            </label>
        </div>
        <div class="text-center">
            Scan QR code on screen to start game
        </div>
        <div class="text-center">
            3/4
        </div>
        <div class="action attribute flex align-center justify-center">
            <div id="link_to_forth_page" class="btn btn-primary">
                {{ trans('instruction.next')}}
            </div>
            <a href="../" class="home-bottom">
                <div class="btn btn-primary">
                    {{ trans('validation.home')}}
                </div>
            </a>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/quagga/1.0.0-beta.1/quagga.js"></script>
@endsection