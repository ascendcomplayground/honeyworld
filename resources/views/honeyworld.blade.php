<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/index.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Honeyworld</title>
</head>

<body>
    <div class="homepage-container">
        <div class="content-container">
            <div class="logo">HONEY WORLD</div>
            <div class="personal-score-description">{{ trans('homepage.total_honey_earned')}}</div>
            <div class="personal-honey-earned">20,123,459</div>
            <img src="{{ asset('/images/Language/Language_sample.png') }}" id="langauge_switcher" usemap="#switcher"></img>
            <map name="switcher">
                <area shape="circle" coords="15,15,30" alt="english" href="lang/en">
                <area shape="circle" coords="45,15,30" alt="chinese" href="lang/zh">
            </map>
            @auth('user')
            <div class="flex welcome">
                <p>{{ trans('homepage.welcome')}}</p>
                <p>{{ trans('homepage.click_for_info')}}</p>
            </div>
            @endauth
        </div>

        <div class="background">
            <div id="outlet" class="carousel slide">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="outlet">Outlet 1</div>
                    </div>

                    <div class="item">
                        <div class="outlet">Outlet 2</div>
                    </div>

                    <div class="item">
                        <div class="outlet">Outlet 3</div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#outlet" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#outlet" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="login-div">
            @auth('user')
            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <div class="button">
                    <img src="{{ asset('/images/Logout Btn.png') }}" class="button-size"></img>
                    <div class="description">Logout</div>
                </div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
            <a href="{{ route('login')}}">
                <div class="button">
                    <img src="{{ asset('/images/Scan Receipt Btn.png') }}" class="button-size"></img>
                    <div class="description">Scan Recipt</div>
                </div>
            </a>
            <a href="./instruction">
                <div class="button">
                    <img src="{{ asset('/images/Play Btn.png') }}" class="button-size"></img>
                    <div class="description">Play</div>
                </div>
            </a>
            @else
            <div class="flex text-center">
                <a href="{{ route('register')}}">
                    <div class="button">
                        <img src="{{ asset('/images/Register BTN.png') }}" class="button-size"></img>
                        <div class="description">Register</div>
                    </div>
                </a>
                <a href="{{ route('login')}}">
                    <div class="button">
                        <img src="{{asset('images/Login Btn.png') }}" class="button-size"></img>
                        <div class="description">Login</div>
                    </div>
                </a>
            </div>
            @endauth
        </div>
    </div>
    <div class="turn">Please rotate your device!</div>
    <script type="text/javascript" src=/js/app.js> </script> </body> </html>