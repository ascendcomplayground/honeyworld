//procedure switching
window.onload = function () {
    $("#link_to_second_page").click(function () {
        console.log("going to second page");
        $(".second_page").show();
        $(".first_page").hide();
    });
    $("#link_to_third_page").click(function () {
        console.log("going to third page");
        $(".third_page").show();
        $(".second_page").hide();
    });
    $("#link_to_forth_page").click(function () {
        console.log("going to forth page");
        $(".third_page").hide();
    });
}

// Barcode_reader
var Quagga = window.Quagga;
var App = {
    _scanner: null,
    init: function () {
        this.attachListeners();
    },
    activateScanner: function () {
        var scanner = this.configureScanner('.overlay__content'),
            onDetected = function (result) {
                document.querySelector('input.isbn').value = result.codeResult.code;
                stop();
            }.bind(this),
            stop = function () {
                scanner.stop();  // should also clear all event-listeners?
                scanner.removeEventListener('detected', onDetected);
                this.hideOverlay();
                this.attachListeners();
            }.bind(this);

        this.showOverlay(stop);
        scanner.addEventListener('detected', onDetected).start();
    },
    attachListeners: function () {
        var self = this,
            button = document.querySelector('.input-field input + button.scan');

        button.addEventListener("click", function onClick(e) {
            e.preventDefault();
            button.removeEventListener("click", onClick);
            self.activateScanner();
        });
    },
    showOverlay: function (cancelCb) {
        if (!this._overlay) {
            var content = document.createElement('div');
            //closeButton = document.createElement('div'); Close button funtion

            //closeButton.appendChild(document.createTextNode('X'));
            content.className = 'overlay__content';
            //closeButton.className = 'overlay__close';
            this._overlay = document.createElement('div');
            this._overlay.className = 'overlay';
            this._overlay.appendChild(content);
            //content.appendChild(closeButton);
            //closeButton.addEventListener('click', function closeClick() {
            //    closeButton.removeEventListener('click', closeClick);
            //    cancelCb();
            //});
            document.getElementById("scanner_layout").appendChild(this._overlay);
        } else {
            //var closeButton = document.querySelector('.overlay__close');
            //closeButton.addEventListener('click', function closeClick() {
            //    closeButton.removeEventListener('click', closeClick);
            //    cancelCb();
            //});
        }
        this._overlay.style.display = "block";
    },
    hideOverlay: function () {
        if (this._overlay) {
            this._overlay.style.display = "none";
        }
    },
    configureScanner: function (selector) {
        if (!this._scanner) {
            this._scanner = Quagga
                .decoder({ readers: ['ean_reader'] }) // can set various type of barcodes here
                .locator({ patchSize: 'medium' }) // can set number of worker, patchSize to small, medium, large
                .fromSource({
                    target: selector,
                    constraints: {
                        width: 640,
                        height: 480,
                        facingMode: "environment" // if front camera the facingMode will be "user"
                    }
                });
        }
        return this._scanner;
    }
};
App.init();

//QR code
document.getElementById("qr_reader").onchange = function () {
    openQRCamera(this);
};

window.openQRCamera = function (node) {
    var reader = new FileReader();
    reader.onload = function () {
        node.value = "";
        qrcode.callback = function (res) {
            if (res instanceof Error) {
                alert("No QR code found. Please make sure the QR code is within the camera's frame and try again.");
            } else {
                node.parentNode.previousElementSibling.value = res;
            }
        };
        qrcode.decode(reader.result);
    };
    reader.readAsDataURL(node.files[0]);
}

